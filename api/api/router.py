import os
import json

import pymongo
from flask import Flask, request
from flask_pymongo import PyMongo
from flask import Response

app = Flask(__name__)

# Configuration part
if os.getenv("MONGO_URI"):
    app.config["MONGO_URI"] = os.environ['MONGO_URI']
else:
    app.config[
        "MONGO_URI"
    ] = "mongodb+srv://dbAtlas:dbAtlasPassword@cluster0-uvhnc.azure.mongodb.net/test?retryWrites=true&w=majority"

mongo = PyMongo(app)


def return_tasks_count() -> int:
    """
    Returns last task ID. We actually look for latest object added, and then
    returns its id.

    If no object is found, returns 0.
    :return:
    """
    task = mongo.db.tasks.find_one(
        sort=[('_id', pymongo.DESCENDING)]
    )
    return int(task['id']) if task else 0


def flatten(element: dict) -> dict:
    """
    Removes unsupported objects like ObjectID from dict received from MongoDB
    mongo.db.collection.find_one(<<query>>) function.

    :param element: dict, record from the MongoDB
    :return: dict without ObjectID so JSON could be handled efficiently
    """
    try:
        element.pop('_id', None)
    except AttributeError:
        pass

    return element


@app.route(
    '/v1.0/tasks/<task_id>',
    methods=[
        "GET", "POST", "DELETE", "PUT", "OPTIONS"
    ]
)
def task_manipulation(task_id: str) -> dict:
    """
    Task manipulation. Remember: slug field is a string so casting is necessary

    :param task_id: slug of task id
    :return: Response object with JSON response and accurate status
    """

    if request.method == "GET":
        """
            Performs getting the element of following id or throwing 404 if
            no such element found.
        """
        if not mongo.db.tasks.find_one({"id": int(task_id)}):
            return Response(
                json.dumps({'description': "There is no such record"}),
                status=404,
                mimetype='application/json'
            )

        return Response(
            json.dumps(
                flatten(mongo.db.tasks.find_one({"id": int(task_id)}))
            ),
            status=200,
            mimetype='application/json'
        )

    elif request.method == "POST":
        """
            Specifies behavior of POST Method.
            
            Throws 405.
        """
        return Response(
            json.dumps({'success': False, "description": "Method not allowed"}),
            status=405,
            mimetype='application/json'
        )

    elif request.method == "PUT":
        """
            Specifies behavior of POST Method.
            
            Looks for the element. If no element is found throws 404.
            If element is found parses input and creates update record
            for mongo Database accordingly.
        """

        if not mongo.db.tasks.find_one({"id": int(task_id)}):
            return Response(
                json.dumps({'description': "There is nothing to delete"}),
                status=404,
                mimetype='application/json'
            )

        data = request.get_json()

        if (
                data.get('title')
                or data.get('description')
                or data.get('done') is not None
        ):
            data['id'] = int(task_id)
            old_record = mongo.db.tasks.find_one({"id": int(task_id)})
            new_record = old_record.copy()

            for k in data.keys():
                new_record[k] = data[k]

            mongo.db.tasks.update_one(
                {'id': int(task_id)},
                {'$set': new_record}
            )

            return Response(
                json.dumps(
                    flatten(mongo.db.tasks.find_one({"id": int(task_id)}))
                ),
                status=202,
                mimetype='application/json'
            )

        else:
            """
                If request is malformed - e.g. there is no field: title, descri-
                ption or done, app will raise 400 bad request.
            """
            return Response(
                json.dumps({'success': False}),
                status=400,
                mimetype='application/json'
            )

    elif request.method == "DELETE":
        """
            Logic of deleting.
            
            First of all, looking for the record. If no record is found 404 is
            thrown.
            
            If there is a record app will delete a record and throw 410 Gone 
            HTTP status
        """
        if not mongo.db.tasks.find_one({"id": int(task_id)}):
            return Response(
                json.dumps({'description': "There is nothing to delete"}),
                status=404,
                mimetype='application/json'
            )

        try:
            """
                Trying to delete...
            """
            mongo.db.tasks.delete_one({"id": int(task_id)})

        except Exception:
            """
                If there is any chance, something will go wrong we will fail
                silently with 500. I know catching bare Exception 
                is a terrible idea. I should have implement this better...
            """
            return Response(status=500)

        return Response(
            json.dumps({'success': True}),
            status=410,
            mimetype='application/json'
        )

    elif request.method == "OPTIONS":
        """
            Bonus. RESTful API shall also return list of possible options.
            We will review the functions better.
        """
        return {
            "options": ["GET", "PUT", "DELETE", "OPTIONS"]
        }

    else:
        """
            If no valid method was provided, we will fall back to 405 Method
            not allowed.
            
            It should be done by the Flask, however, just in case if something
            goes wrong we should handle this...
        """
        return Response(
            json.dumps(
                {"success": False, "description": "405 Method not allowed"}
            ),
            status=405,
            mimetype='application/json'
        )


@app.route('/v1.0/tasks/', methods=["GET", "POST"])
def tasks_home() -> dict:
    """
    Handles main endpoint of the application.

    :return: JSON response with either list of tasks, ID of a newly created task
    or JSON response with error
    """
    if request.method == "GET":
        """
            Lets get a list of elements here.
            
            Using flatten() function (which prevents json built-in library to
            fail in case of finding ObjectID class, we will strip it before sen-
            ding to json.dumps(tasks)
        """
        try:
            tasks = [flatten(element) for element in
                     list(mongo.db.tasks.find())]

            return Response(
                json.dumps({'tasks': tasks}),
                status=200,
                mimetype='application/json'
            )
        except Exception as e:
            """
                FIXME: Handle exceptions
                
                I know. This is more than terribly ugly. Will fix that later.
                We will handle possible exceptions from MongoDB.
            """
            # FIXME: Handle exceptions
            return Response(
                json.dumps(
                    {
                        "success": "false",
                        "description": "Unprocessable Entity",
                        "cause" : str(e)
                    }
                ),
                status=422,
                mimetype='application/json'
            )

    elif request.method == "POST":
        """
            If there is a POST request we will handle creation.
        """
        data = request.get_json()  # type: dict

        # First, let's check there are required fields.
        if (
                data.get('title')
                and data.get('description')
                and data.get('done') is not None
        ):
            try:
                dct = dict()
                # in case there are other data let's simply strip them.
                # We don't want trash in our database.
                # Mongo won't complain, however if we change DB to Oracle or PG
                # bad things may happen...
                for key in ['title', 'description', 'done']:
                    dct[key] = data[key]

                # query last element and get its id
                dct['id'] = return_tasks_count() + 1

                # insert element and return its id
                # Remember, we're not returning Mongo's _id !!! That's a whole
                # different thing. We are managing independently our own
                # artificial key.
                mongo.db.tasks.insert_one(dct)
                return Response(
                    json.dumps({'success': True, 'id': dct['id']}),
                    status=201,
                    mimetype='application/json'
                )

            except Exception as e:
                # yes, I know this is terribly ugly, but we'll fix that later
                # FIXME: Handle exceptions
                return Response(
                    json.dumps(
                        {
                            "success": False,
                            "description": "Unprocessable Entity",
                            "error": str(e)
                        }
                    ),
                    status=422,
                    mimetype='application/json'
                )
        else:
            """
                If the request won't contain necessary fields we will raise
                400 Bad Request HTTP code to the client. 
            """
            return Response(
                json.dumps(
                    {
                        "success": False,
                        "description": "400 Bad Request",
                        "additional_info": "One of the required fields was not found in the request.",
                        "required_fields": ["done", "description", "title"]
                    }
                ),
                status=400,
                mimetype='application/json'
            )

    else:
        """
            In case of methods that we don't actually support
            
            This is actually handled by Flask but. Just in case.
        """
        return Response(
            json.dumps(
                {
                    "success": False,
                    "description": "Method not allowed",
                }
            ),
            status=405,
            mimetype='application/json'
        )


if __name__ == '__main__':
    app.run()
